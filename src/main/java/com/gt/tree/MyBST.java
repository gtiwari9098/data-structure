package com.gt.tree;

public class MyBST {
	MyNode root ;
	 public void insert(int data) {
		 System.out.println("inserting "+data);
		 MyNode newNode = new MyNode(data);
		 
		 if(root == null) {
			 root = newNode;
			 return;
		 }else {
			 MyNode current = root;
			 MyNode parent = null;
			 while(true) {	
				 parent = current;
			 if ( data < parent.data ) {
				 current  = parent.left;
				 if(current == null) {
					 parent.left = newNode;
					 return;
				 }
				 
			 } else {
				current = current.right;
				if(current == null) {
					parent.right = newNode;
					return;
				}
			 }
			 
		 }
		 }
		 
		 
		 
		 
	 }

	 
	 public void inorderTraversal(MyNode node) {
		 if (root == null) {
			 System.out.println("tree is empty");
			 return ;
		 } else {
			 System.out.println("parent node  "+node.data);
			 if(node.left != null) {
				 inorderTraversal(node.left);
				 System.out.println(node.left.data);
			 }if(node.right != null) {
				 inorderTraversal(node.right);
				 System.out.println(node.right.data); 
			 }
		 }
	 }
	 public static void main (String[] args) {
		 
		 MyBST bt = new MyBST();
	        //Add nodes to the binary tree
	        bt.insert(50);
	        bt.insert(30);
	        bt.insert(70);
	        bt.insert(60);
	        bt.insert(10);
	        bt.insert(90);

	        System.out.println("Binary search tree after insertion:");
	        //Displays the binary tree
	        bt.inorderTraversal(bt.root);
		 
	 }
	 
}



class MyNode {
	int data;
	MyNode left;
	MyNode right;
	
	MyNode(int data){
		this.data = data;
		this.left = null;
		this.right = null;
	}
}