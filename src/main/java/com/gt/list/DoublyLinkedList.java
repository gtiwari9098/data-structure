package com.gt.list;

public class DoublyLinkedList {

    public DoublyNode head = null;
    public DoublyNode tail = null;

    public static void main(String[] args) {

        DoublyLinkedList sList = new DoublyLinkedList();

        //Add DoublyNodes to the list
        sList.addDoublyNode(1);
        sList.addDoublyNode(2);
        sList.addDoublyNode(3);
        sList.addDoublyNode(4);

        //Displays the DoublyNodes present in the list
        sList.display();
    }

    //addDoublyNode() will add a new DoublyNode to the list
    public void addDoublyNode(int data) {
        //Create a new DoublyNode
        DoublyNode newDoublyNode = new DoublyNode(data);

        //Checks if the list is empty
        if (head == null) {
            //If list is empty, both head and tail will point to new DoublyNode
            head = newDoublyNode;
            tail = newDoublyNode;
            head.previous = null;
            tail.next = null;
        } else {
            //newDoublyNode will be added after tail such that tail's next will point to newDoublyNode
            tail.next = newDoublyNode;
            //newDoublyNode will become new tail of the list
            tail = newDoublyNode;
        }
    }
    // display() will display all the DoublyNodes present in the list
    public void display() {
        //DoublyNode current will point to head
        DoublyNode current = head;

        if (head == null) {
            System.out.println("List is empty");
            return;
        }
        System.out.println("DoublyNodes of singly linked list: ");
        while (current != null) {
            //Prints each DoublyNode by incrementing pointer
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();
    }
}
class DoublyNode{
    int data;
    DoublyNode next;
    DoublyNode previous;
    public DoublyNode(int data) {
        this.data = data;
        this.next = null;
    }
}
