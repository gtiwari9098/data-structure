package com.gt.problems;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Find the length  of shortest substring form the string which contains all unique characters

 */
public class FindMaxNumberOfcharacterSequence {
	public static void main(String[] args) {

		System.out.println(FindMaxNumberOfcharacterSequence.getCountOfShortestSubString("ababab", 2, 3, 4));
	}

	private static int getCountOfShortestSubString(String str, int minLength, int maxLength, int maxUnique) {
		// get unique characters list/array from the string
		List<String> distinctCharList = str.chars().mapToObj(e -> Character.toString((char) e)).distinct()
				.collect(Collectors.toList());

		List<String> subStringList = new ArrayList<>();
		for (int i = 0; i < str.length(); i++) {
			for (int j = i + 1; j <= str.length(); j++) {
				String firstPart = str.substring(i, j);
				// only add substring when atleast all number of characters may exist in the
				// substrings
				subStringList.add(firstPart);
							}
		}

		// filter the substring by provided minLenght and maxLength
		Map<String, Long> finalList = subStringList.stream()
				.filter(stre -> ((stre.length() >= minLength) && (stre.length() <= maxLength)))
				.filter(substr -> substr.chars().mapToObj(o -> (char)o ).distinct().collect(Collectors.toList()).size() <= maxUnique)
				// .collect(Collectors.toList());
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

		
		long max = finalList.values().stream().max(Comparator.comparing(Function.identity())).orElse(0l);
		distinctCharList.forEach(System.out::println);
		
		/////
		Map<String, Long> finalMap = new LinkedHashMap();
		finalList.entrySet().stream()
        .sorted(Map.Entry.<String, Long>comparingByValue()
                .reversed()).forEachOrdered(e -> finalMap.put(e.getKey(), e.getValue()));

		
		
		/////////////
		// System.out.println(groupByPriceMap);

		// group by price, uses 'mapping' to convert List<Item> to Set<String>
       // Map<BigDecimal, Set<String>> result =
        //        items.stream().collect(
         //               Collectors.groupingBy(Item::getPrice,
          //                      Collectors.mapping(Item::getName, Collectors.toSet())
           //             )
            //    );
        
        
        ////////////////
        //Map<String, Long> counting = items.stream().collect(
        //        Collectors.groupingBy(Item::getName, Collectors.counting()));

        //System.out.println(counting);

        //Map<String, Integer> sum = items.stream().collect(
         //       Collectors.groupingBy(Item::getName, Collectors.summingInt(Item::getQty)));

		return 0;
	}
}