package com.gt.problems;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Find the length  of shortest substring from the string which contains all unique characters

 */
public class FIindCharacterStringProblem {
    public static void main(String [] args)
    {

        System.out.println(FIindCharacterStringProblem.getCountOfShortestSubString("acbbdbcadbbbca"));
    }

    private static int getCountOfShortestSubString(String str){
        // get unique characters list/array from the string
        Set<String>  distinctCharList = str.chars()
                .mapToObj(e -> Character.toString((char)e))
                .distinct()
                .collect(Collectors.toSet());

        Set<String> subStringList = new HashSet<>();
        for ( int i=0; i<str.length() ; i++) {
            for( int j =i+1 ;j <= str.length();j++) {
                    String firstPart = str.substring(i, j);
                    // only add substring when atleast all number of characters may exist in the substrings
                    if (firstPart.length() >= distinctCharList.size()) {
                        subStringList.add(firstPart);
                    }
            }
        }
           List<String> finalList = subStringList.stream()
                    .filter(substr -> distinctCharList
                            .stream().allMatch(charstr -> substr.contains(charstr))).collect(Collectors.toList());
            finalList.forEach(System.out::println);

        distinctCharList.forEach(System.out::println);
        return 0;
    }
}