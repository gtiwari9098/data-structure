package com.gt.problems;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/*
 * 
 */
public class StreamGroupByExample {

	public static void main(String[] args) {

		List<Trade> tradeList = new ArrayList<Trade>();

		tradeList.add(new Trade(11, 100.0));
		tradeList.add(new Trade(11, 1000.0));
		tradeList.add(new Trade(13, 200.0));
		tradeList.add(new Trade(14, 600.0));
		tradeList.add(new Trade(15, 800.0));
		tradeList.add(new Trade(16, 900.0));
		tradeList.add(new Trade(13, 100.0));

		// find the max trade value of
		Map<Trade, Double> tradeMap = tradeList.stream()
				.collect(Collectors.toMap(Function.identity()
						, Trade::getTradeValue
						,(t1,t2)->t1>t2?t1:t2));

		System.out.println(tradeMap.toString());
		// find the map entry with highestvalue
		Map.Entry<Trade, Double> maxEntry = tradeMap.entrySet().stream()
				  .max(Map.Entry.comparingByValue()).get();
		System.out.println(maxEntry.toString());
	}

}

class Trade {
	private Integer tradeId;
	private Double tradeValue;

	public Trade(Integer tradeId, Double tradeValue) {
		super();
		this.tradeId = tradeId;
		this.tradeValue = tradeValue;
	}

	public Integer getTradeId() {
		return tradeId;
	}

	public void setTradeId(Integer tradeId) {
		this.tradeId = tradeId;
	}

	public Double getTradeValue() {
		return tradeValue;
	}

	public void setTradeValue(Double tradeValue) {
		this.tradeValue = tradeValue;
	}

	@Override
	public String toString() {
		return "Trade [tradeId=" + tradeId + ", tradeValue=" + tradeValue + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tradeId == null) ? 0 : tradeId.hashCode());
		//result = prime * result + ((tradeValue == null) ? 0 : tradeValue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trade other = (Trade) obj;
		if (tradeId == null) {
			if (other.tradeId != null)
				return false;
		} else if (!tradeId.equals(other.tradeId))
			return false;

		/*
		 * if (tradeValue == null) { if (other.tradeValue != null) return false; } else
		 * if (!tradeValue.equals(other.tradeValue)) return false;
		 */

		return true;
	}

}
