package com.gt.problems;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FindNumberEvenOddProblem {

	public static void main(String[] args) {
		
		retrieveOddNumbersWithinRange(10, 25);
		 
        retrieveEvenNumbersWithinRange(10, 25);
	}
	 public static void retrieveOddNumbersWithinRange(int start, int end) {
	        System.out.println("ODD NUMBERS WITHIN THE RANGE OF 10 AND 25");
	 
	        //1st way
	        System.out.println("\n 1st way: \n");
	        IntStream.range(start, end + 1).filter(i -> i % 2 != 0).forEach(System.out::println);
	 
	      
	        //3rd way
	        System.out.println("\n 3rd way: \n");
	        Stream.iterate(start, i -> i + 1)
	        .filter(FindNumberEvenOddProblem::isOdd).limit(8).forEach(System.out::println);
	    }
	 
	    public static void retrieveEvenNumbersWithinRange(int start, int end) {
	        System.out.println("\n\n EVEN NUMBERS WITHIN THE RANGE OF 10 AND 25");
	 
	        //1st way
	        System.out.println("\n 1st way: \n");
	        IntStream.range(start, end + 1).filter(i -> i % 2 == 0).forEach(System.out::println);
	
	 
	        //3rd way
	        System.out.println("\n 3rd way: \n");
	        Stream.iterate(start, i -> i + 1).filter(FindNumberEvenOddProblem::isEven).limit(8).forEach(System.out::println);
	    }
	 
	    public static boolean isOdd(int number) {
	        return number % 2 != 0;
	    }
	 
	    public static boolean isEven(int number) {
	        return number % 2 == 0;
	    }
}
