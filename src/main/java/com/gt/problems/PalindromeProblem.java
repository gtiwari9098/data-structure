package com.gt.problems;

import java.util.stream.IntStream;

public class PalindromeProblem {
public static void main(String[] args) {
	
}


static void checkPalindromInJava8(String inputString) {
    
    //Java 8 code to check for palindrome
     
    boolean isItPalindrome = IntStream.range(0, inputString.length()/2).
            noneMatch(i -> inputString.charAt(i) != inputString.charAt(inputString.length() - i -1));
     
    if (isItPalindrome)
    {
        System.out.println(inputString+" is a palindrome");
    }
    else
    {
        System.out.println(inputString+" is not a palindrome");
    }
}
static void palindromeUsingStringBuffer(String inputString) {
	System.out.println("Enter input String :");
    
   
     
    //Reverse inputString using reverse() method of StringBuffer
     
    String reverseString = new StringBuilder(inputString).reverse().toString();
     
    //Check inputString and reverseString for equality
     
    if (inputString.equalsIgnoreCase(reverseString))
    {
        System.out.println(inputString+" is a palindrome");
    }
    else
    {
        System.out.println(inputString+" is not a palindrome");
    }
     
}
}
