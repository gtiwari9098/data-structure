package com.gt.problems;

import java.math.BigInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FindPrimeNumberProblem {
	public static void main(String[] args) {

		long count = Stream.iterate(0, n -> n + 1).limit(1000).filter(FindPrimeNumberProblem::isPrime)
				.peek(x -> System.out.format("%s\t", x)).count();

		System.out.println("\nTotal: " + count);
	}

	public static boolean isPrime(int number) {

		if (number <= 1)
			return false; // 1 is not prime and also not composite

		return IntStream.rangeClosed(2, number / 2).noneMatch(i -> number % i == 0);
	}
	
	// in java 9 inbuiltmethod
	public static void findNextProbablePrime() {
		Stream.iterate(BigInteger.valueOf(2), BigInteger::nextProbablePrime)
		.limit(168)
		.forEach(x -> System.out.format("%s\t", x));
		
		// another way
		/*
		 * Stream.iterate(BigInteger.TWO, BigInteger::nextProbablePrime) .limit(168)
		 * .forEach(x -> System.out.format("%s\t", x));
		 */
	}
}
